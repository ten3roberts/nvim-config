let g:lua_tree_git_hl = 1

" Use a custom function instead due to startify
let g:lua_tree_auto_open = 0
let g:lua_tree_follow = 1
let g:lua_tree_indent_markers = 1

let lua_tree_show_icons = {
            \ 'git': 0,
            \ 'folders': 1,
            \ 'files': 1,
            \}

let g:lua_tree_icons = {
            \ 'default': '',
            \ 'symlink': '',
            \ 'git': {
            \   'unstaged': "•",
            \   'staged': "✓",
            \   'unmerged': "",
            \   'renamed': "➜",
            \   'untracked': "+"
            \   },
            \ 'folder': {
            \   'default': "",
            \   'open': ""
            \   }
            \ }
let g:lua_tree_bindings = {
            \ 'edit':              [ '<Tab>', '<Cr>' ],
            \ 'cd':              'cd',
            \ }
            " \ 'edit':            ['<CR>', '<TAB>', 'o', 'l'],
"===============================================================================
" Highlights                                                                 {{{

hi! LuaTreeNormal      guifg=#e0e0e0 guibg=#24292e gui=none
hi! FolderInverted     guifg=#e0e0e0
hi! FileInverted       guifg=#e0e0e0

hi! CursorLineInverted guibg=#343d45

hi! LuaTreeRootFolder  guifg=#e2c08d gui=bold

hi! LuaTreeFolderName  guifg=#e0e0e0 gui=bold
hi! LuaTreeFolderIcon  guifg=#e2c08d

hi! LuaTreeGitRenamed  guifg=#61afef
hi! LuaTreeGitNew      guifg=#73c990
hi! LuaTreeGitMerge    guifg=#73c990
hi! LuaTreeGitModified guifg=#e2c08d
hi! LuaTreeGitDirty    guifg=#e2c08d

hi! link LuaTreeEndOfBuffer  TabLineFill
hi! link LuaTreeCursorLine   CursorLineInverted

hi! link LuaTreeFolderName   FolderInverted
hi! link LuaTreeSymlink      FileInverted
hi! link LuaTreeExecFile     FileInverted
hi! link LuaTreeSpecialFile  FileInverted
hi! link LuaTreeImageFile    FileInverted
hi! link LuaTreeMarkdownFile FileInverted
