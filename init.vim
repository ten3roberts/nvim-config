" Plugin Section
call plug#begin('~/.vim/plugged')

" Plug 'Yggdroot/indentLine'
" Plug 'airblade/vim-rooter'
" Plug 'cocopon/vaffle.vim'
" Plug 'nvim-treesitter/playground'
" Plug 'preservim/nerdtree'
" Plug 'qxxxb/vim-searchhi'
Plug 'AndrewRadev/sideways.vim'
Plug 'airblade/vim-gitgutter'
Plug 'akinsho/nvim-toggleterm.lua'
Plug 'cespare/vim-toml'
Plug 'https://github.com/sbdchd/neoformat'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'junegunn/vim-easy-align'
Plug 'justinmk/vim-sneak'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'mhinz/vim-startify'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'psliwka/vim-smoothie'
Plug 'rhysd/git-messenger.vim'
Plug 'romgrk/barbar.nvim'
Plug 'roryokane/detectindent'
Plug 'roxma/vim-window-resize-easy'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'steelsojka/completion-buffers'
Plug 'szw/vim-maximizer'
Plug 'tikhomirov/vim-glsl'
Plug 'tmsvg/pear-tree'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'wellle/targets.vim'
Plug 'zah/nim.vim'

" Color schemes
Plug 'arcticicestudio/nord-vim'
Plug 'ayu-theme/ayu-vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'gruvbox-community/gruvbox'
Plug 'joshdick/onedark.vim'
Plug 'rakr/vim-one'
Plug 'romgrk/doom-one.vim'

" nvim-lsp
" Collection of common configurations for the Nvim LSP client
Plug 'neovim/nvim-lspconfig'

" Extensions to built-in LSP, for example, providing type inlay hints
Plug 'ten3roberts/lsp_extensions.nvim', {'branch': 'fix_inlay_hints_badarg'}

" Autocompletion framework for built-in LSP
Plug 'nvim-lua/completion-nvim'

Plug 'RishabhRD/popfix'
Plug 'RishabhRD/nvim-lsputils'
Plug 'nvim-lua/lsp-status.nvim'

call plug#end()

let s:rcpath = expand('<sfile>:p:h')
let g:vimrc = expand('<sfile>:p')

" Source file
function! s:load(file)
    let file = s:rcpath . '/' . a:file
    execute (l:file =~# '.lua$' ? 'luafile' : 'source') fnameescape(l:file)
endfunction

" Split configuration
call s:load('settings.vim')

call s:load('autocommands.vim')
call s:load('keymap.vim')
call s:load('fzf.vim')
call s:load('lsp_settings.vim')
call s:load('lua-tree.vim')
call s:load('startify.vim')

" call s:load('vaffle.vim')
call s:load('lightline.vim')

call s:load('winhl.vim')
call s:load('scripts/bufonly.vim')
call s:load('scripts/bool.vim')

lua require "init"

function! AlternateFile()
    return expand('#')
endfunction

function! SetColors()
    " Color scheme
    if (has("termguicolors"))
        " syntax enable
        set termguicolors
    endif

    if &background == "light"
        colorscheme $VIM_LIGHT_COLORSCHEME
    else
        colorscheme $VIM_DARK_COLORSCHEME
    endif
endfunction

call SetColors()

" Fold, gets it's own section  ----------------------------------------------{{{
function! MyFoldText() " {{{
    let line = getline(v:foldstart)
    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    " let fillcharcount = windowwidth - len(line) - len(foldedlinecount) - len('lines')
    " let fillcharcount = windowwidth - len(line) - len(foldedlinecount) - len('lines   ')
    let fillcharcount = windowwidth - len(line)
    " return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . ' Lines'
    return line . ''. repeat(" ",fillcharcount)
endfunction " }}}

function! JumpInFile(back, forw)
    let [n, i] = [bufnr('%'), 1]
    let p = [n] + getpos('.')[1:]
    sil! exe 'norm!1' . a:forw
    while 1
        let p1 = [bufnr('%')] + getpos('.')[1:]
        if n == p1[0] | break | endif
        if p == p1
            sil! exe 'norm!' . (i-1) . a:back
            break
        endif
        let [p, i] = [p1, i+1]
        sil! exe 'norm!1' . a:forw
    endwhile
endfunction

command! Reload execute "source" g:vimrc

command! Indent normal! mggg=G`g

let g:original_window_title = system("xtitle")
call system("set_title Neovim")
