" returns all modified files of the current git repo
" `2>/dev/null` makes the command fail quietly, so that when we are not
" in a git repo, the list will be empty
function! s:gitModified()
    let files = systemlist('git ls-files -m 2>/dev/null')
    return map(files, "{'line': v:val, 'path': v:val}")
endfunction

" same as above, but show untracked files, honouring .gitignore
function! s:gitUntracked()
    let files = systemlist('git ls-files -o --exclude-standard 2>/dev/null')
    return map(files, "{'line': v:val, 'path': v:val}")
endfunction

function! List_projects() abort
  return map(finddir('.git', '~/dev/**', -1),
        \ {_, dir -> {'line': fnamemodify(dir, ':h:t'), 'path': fnamemodify(dir, ':h')}})
endfunction

" let g:startify_custom_header = startify#pad(split(system('echo "Neovim" | figlet'), '\n') + startify#fortune#boxed())
let g:startify_custom_header = startify#pad(split(system('figlet Neovim'), '\n'))

let g:startify_bookmarks = [{'s': '~/dev/sprocket'}, {'d': '~/dev/'}, {'n': '~/.config/nvim'}, {'z': '~/.config/zsh/.zshrc'}]

let g:startify_session_autoload = 1
let g:startify_session_persistence = 1

let g:startify_lists = [
            \ { 'type': 'dir',       'header': ['   Recent Files ' . getcwd()] },
            \ { 'type': 'sessions',  'header': ['   Sessions']       },
            \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
            \ { 'type': function('s:gitModified'),  'header': ['   Git Modified']},
            \ { 'type': function('s:gitUntracked'), 'header': ['   Git Untracked']},
            \ { 'type': 'files',     'header': ['   MRU']            },
            \ { 'type': 'commands',  'header': ['   Commands']       },
            \ ]

let g:startify_session_before_save = [
    \ 'echo "Cleaning up before saving ..."',
    \ 'silent! LuaTreeClose'
    \ ]

let g:startify_center = 50
let g:startify_files_number = 10
let g:indentLine_fileTypeExclude = ['startify']

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe "cd " .argv()[0] | exe "pwd" | exe 'Startify' | endif
