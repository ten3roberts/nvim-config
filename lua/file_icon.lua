local nvim_devicons = require 'nvim-web-devicons'

function get_file_icon(filename)
    filename = vim.fn.expand(filename)
    local ext = vim.fn.fnamemodify(filename, ":e")
    local basename = vim.fn.fnamemodify(filename, ":t")

    return nvim_devicons.get_icon(basename, ext, { default = true })
end
