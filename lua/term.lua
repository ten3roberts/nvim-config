local toggleterm = require "toggleterm"

toggleterm.setup{
        size = 15,
        open_mapping = [[<C-t>]],
        shade_filetypes = {"none"},
        shade_terminals = true,
        direction = 'horizontal'
    }

