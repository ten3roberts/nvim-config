local nvim_lsp = require'lspconfig'
local lsp_status = require'lsp-status'

local M = {}

local indicators = {
    kind_labels = {},
    errors = '',
    warnings = '',
    info = '',
    hint = '❗',
    ok = '',
    frames = { '⣾', '⣽', '⣻', '⢿', '⡿', '⣟', '⣯', '⣷' },
    status_symbol = ' 🇻',
    select_symbol = nil
}

local function filter_symbol(symbol)
    local kind = symbol.kind
    if kind == "TypeParameter" then return false end

    return true
end

function M.get_document_symbols()
    local opts = {}

    local params = vim.lsp.util.make_position_params()
    local results_lsp = vim.lsp.buf_request_sync(0, "textDocument/documentSymbol", params, opts.timeout or 1000)

    if not results_lsp or vim.tbl_isempty(results_lsp) then
        print("No results from textDocument/documentSymbol")
        return
    end

    local items = {}
    for _, server_results in pairs(results_lsp) do
        vim.list_extend(items, vim.lsp.util.symbols_to_items(server_results.result, 0) or {})
    end

    if vim.tbl_isempty(items) then
        return
    end

    local lines = {}
    for _, item in pairs(items) do
        if filter_symbol(item) then
            lines[#lines + 1] = string.format("%d    %s", item.lnum, item.text)
        end
    end
    return lines
end

function M.get_workspace_symbols()
    local opts = {}

    local params = vim.lsp.util.make_position_params()
    local results_lsp = vim.lsp.buf_request_sync(0, "workspace/symbol", { query = '' }, opts.timeout or 1000)

    if not results_lsp or vim.tbl_isempty(results_lsp) then
        print("No results from workspace/symbol")
        return
    end

    local items = {}
    for _, server_results in pairs(results_lsp) do
        vim.list_extend(items, vim.lsp.util.symbols_to_items(server_results.result, 0) or {})
    end

    if vim.tbl_isempty(items) then
        return
    end

    local lines = {}
    for _, item in pairs(items) do
        if filter_symbol(item) then
            lines[#lines + 1] = string.format("%s:%d %s", vim.fn.fnamemodify(item.filename, ":."), item.lnum, item.text)
        end
    end
    return lines
end

-- Prettier code actions and code references
vim.lsp.callbacks['textDocument/codeAction'] = require'lsputil.codeAction'.code_action_handler
vim.lsp.callbacks['textDocument/references'] = require'lsputil.locations'.references_handler
vim.lsp.callbacks['textDocument/definition'] = require'lsputil.locations'.definition_handler
vim.lsp.callbacks['textDocument/declaration'] = require'lsputil.locations'.declaration_handler
vim.lsp.callbacks['textDocument/typeDefinition'] = require'lsputil.locations'.typeDefinition_handler
vim.lsp.callbacks['textDocument/implementation'] = require'lsputil.locations'.implementation_handler
vim.lsp.callbacks['textDocument/documentSymbol'] = require'lsputil.symbols'.document_handler
vim.lsp.callbacks['workspace/symbol'] = require'lsputil.symbols'.workspace_handler

-- vim.lsp.callbacks['textDocument/signatureHelp'] = function(...)
--     print("Signature helpt wanted", ...)
-- end

-- Diagnostics
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
        -- Enable underline, use default values
        underline = true,
        -- Enable virtual text, override spacing to 4
        virtual_text = {
            spacing = 8,
            prefix = '',
        },
        signs = true,
        update_in_insert = false,
    })

-- Override hover winhighlight.
local method = 'textDocument/hover'
local hover = vim.lsp.callbacks[method]
vim.lsp.callbacks[method] = function (_, method, result)
    hover(_, method, result)

    for _, winnr in ipairs(vim.api.nvim_tabpage_list_wins(0)) do
        if pcall(function ()
                    vim.api.nvim_win_get_var(winnr, 'textDocument/hover')
                end) then
            vim.api.nvim_win_set_option(winnr, 'winhighlight', 'Normal:DarkenedPanel')
            break
            else
            -- Not a hover window.
        end
    end
    end

    -- function to attach completion and diagnostics
    -- when setting up lsp
    local on_attach = function(client)
        -- diagnostic.on_attach(client)

        lsp_status.on_attach(client)
    end


    local servers = {
        pyls={},
        sumneko_lua={},
        rust_analyzer={},
        vimls={},
        jsonls={vim.fn.getcwd},
        nimls={},
        hls={},
    }

    function GetLspStatus()
        local diagnostics = lsp_status.diagnostics()

        local str = {}
        str[#str + 1] = indicators.errors .. ' ' .. (diagnostics.errors or 0)
        str[#str + 1] = indicators.warnings .. ' ' .. (diagnostics.warnings or 0)
        str[#str + 1] = indicators.info .. ' ' .. (diagnostics.info or 0)

        str = table.concat(str, ' ')

        return str
    end

    local function setup_server(name, config)
        local full_cfg = vim.tbl_extend("force", { on_attach=on_attach }, config)
        nvim_lsp[name].setup(full_cfg);
    end

    local function setup_servers(servers)
        for k, v in pairs(servers) do
            setup_server(k, v)
        end
    end

    setup_servers(servers)

    -- -- Enable rust_analyzer
    -- nvim_lsp.rust_analyzer.setup({ on_attach=on_attach,
    --         settings={["rust-analyzer"]=
    --             {
    --                 completion = { addCallArgumentSnippets = false, addCallParenthesis = false }
    --             }
    --         }
    --     })
    return M
