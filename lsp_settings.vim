function! LspStatus() abort
    return luaeval('GetLspStatus()')
endfunction

" Set updatetime for CursorHold
" 300ms of no cursor movement to trigger CursorHold
set updatetime=300
set signcolumn=yes

" Show diagnostic popup on cursor hold
" autocmd CursorHold * lua vim.lsp.util.show_line_diagnostics()

" have a fixed column for the diagnostics to appear in
" this removes the jitter when warnings/errors flow in

" Completion
set pumheight=10

" Less aggressive autocomplete
let g:completion_timer_cycle = 200
" let g:completion_trigger_character = [',']
let g:completion_sorting = 'none'
" let g:completion_matching_strategy_list = ['exact', 'substring']
let g:completion_menu_length = 60
" let g:completion_trigger_keyword_length = 1
let g:completion_enable_auto_signature = 1
let g:completion_enable_snippet = 'vim-vsnip'

let g:completion_chain_complete_list = {
            \ 'rust': [
            \{'complete_items': ['lsp', 'snippet', 'path']},
            \],
            \ 'default': [
            \{'complete_items': ['lsp', 'snippet', 'buffer', 'path']},
            \{'mode': '<c-p>'},
            \{'mode': '<c-n>'}
            \]}

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect

" Avoid showing message extra message when using completion
set shortmess+=c


let g:float_preview#winhl="Normal:DarkenedPanel"
