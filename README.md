# Neovim Config
This repository contains my configuration for the Neovim editor
Feel free to use this and take inspiration

This is however not a professional or serious repository, I only use it to backup and share my configuration across computers

Things may break often as I change around and move stuff

Bindings hop around and plugins fly

This will however, be as up to date as possible, to my current Neovim configuration
