let g:vaffle_show_header=1
let g:vaffle_auto_cd=1

function! s:customize_vaffle_mappings() abort
    " Customize key mappings here
    nmap  <buffer> <Bslash> <Plug>(vaffle-open-root)
    nmap  <buffer> a        <Plug>(vaffle-new-file)
    nmap  <buffer> <C-v>    <Plug>(vaffle-open-selected-vsplit)
    map   <buffer> ,        <Plug>(vaffle-toggle-current)
endfunction

augroup vaffle_keymap
    autocmd!
    autocmd FileType vaffle call s:customize_vaffle_mappings()
augroup end
